const App = {
    data() {
        return {
            placeholder: "Название заметки...",
            inputValue: "",
            notes: [
                "Заметка 1",
            ]
        }
    },
    methods: {
        addNewNoteHandler() {
            if (this.inputValue.trim()) {
                this.notes.push(`${this.inputValue}`);
                this.inputValue = "";
                this.placeholder = "Название заметки...";
            } else {
                this.placeholder = "Должно быть не менее одного символа!"
            }
        },
        deleteNote(selected_note) {
            this.notes = this.notes.filter((note) => note !== selected_note);
        }
    },
    computed: {
        doubleCount() {
            console.log(123);
            return this.notes.length * 2;
        }
    },
    watch: {
        inputValue(val) {
            if (val.length > 10) {
                this.inputValue = ""
            }
        }
    },
}

Vue.createApp(App).mount("#root")